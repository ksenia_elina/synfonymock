<?php
    namespace App\Controller;
    
    interface IApiClient
    {
        public function getResourceByPath(string $path): array;
    }