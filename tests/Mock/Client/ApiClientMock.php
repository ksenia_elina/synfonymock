<?php
    namespace App\Tests\Mock\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

    class ApiClientMock extends AbstractController
    {
        private $mockFolder =  '\resources\api';

        public function index(): Response
        {
            $obj = $this->getResourceByPath('article');
            return new Response(serialize($obj));
        }
        
        public function getResourceByPath(string $path): object
        {
            $localFile = sprintf(
              '%s\%s.json',
              dirname(dirname(dirname(__FILE__))).$this->mockFolder, 
              $path
            );

            return json_decode(
                file_get_contents($localFile)
            );
        }
    }